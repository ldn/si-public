#!/usr/bin/perl

# Recuperation des graphiques de consommation de FDN a partir des identifiants
# d'adherent du FAI en marque blanche (depuis le 31/10/11, l'identification avec
# le login radius d'un ligne n'est plus possible).
#
# USAGE: ./% prenom nom

use strict;
use warnings;
use WWW::Mechanize;

my $mech = WWW::Mechanize->new(
	agent => 'SI-LDN'
);

if(@ARGV != 2) {
	print "Usage: $0 prenom nom\n";
	exit 1;
}

# Identifiants d'adherent du FAI en marque blanche
my $adh_login = '';
my $adh_pwd = '';

# Abonne concerne
my $prenom = $ARGV[0];
my $nom = $ARGV[1];

# Nom composes, voir https://repo.ldn-fai.net/redmine/issues/100
$nom =~ s/ /./g;

eval {
	# Identification sur le SI de FDN
	$mech->get("https://vador.fdn.fr/adherents/index.cgi?adhlogin=$adh_login&adhpasswd=$adh_pwd&do=yes");
};

if($@) {
	print "<div id='error'>Le service semble indisponible actuellement,<br />la version des graphs consultés dernièrement est affichée.</div>";
	exit 1;
}

# Recuperation de l'ID de session dans l'URL de redirection (aucun cookie)
my $graphs_uri = $mech->uri();

# Il faut obligatoirement passer par cette page pour faire generer les graphs et
# s'eviter un "session invalide"
$graphs_uri =~ s/-in/-stats/;
$mech->get($graphs_uri);

# Recuperation du graph de conso totale
$graphs_uri =~ s/-stats/-statimg/;
$mech->get($graphs_uri."&img=total.rrd&freq=daily", ':content_file' => "total.png");
$mech->get($graphs_uri."&img=total-fai-locaux.rrd&freq=daily", ':content_file' => "failocaux.png");
$mech->get($graphs_uri."&img=moyenne&freq=daily", ':content_file' => "moyenne.png");

# Recuperation des graphs individuels
$graphs_uri .= "&img=$prenom.$nom%40fdn.nerim.rrd&freq=";

for (qw(daily weekly monthly yearly)) {
	$mech->get($graphs_uri.$_, ':content_file' => "$_.png");
}

exit 0;
